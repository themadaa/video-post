@extends('layouts.main')
@section('title', 'Posts')

@section ('content')
    @include('includes._header', [
        'title' => 'Posts list',
        'second_link' => false,
        'current_page' => 'Posts list',
        'create_block' => true,
        'create_link_route' => route('posts.create'),
        'edit_block' => false,
        'delete_block' => false,
    ])

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Post title</th>
                                <th>Post text</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @each('post._post-list', $posts, 'post', 'post._post-empty')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection