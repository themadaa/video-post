@extends('layouts.main')
@section('title', 'Post edit')

@section ('content')
    @include('includes._header', [
        'title' => 'Post edit: ' . $post->title,
        'second_link' => true,
        'second_link_name' => 'Posts',
        'second_link_route' => route('posts.index'),
        'current_page' => 'Post edit',
        'create_block' => false,
        'edit_block' => false,
        'delete_block' => false,
    ])

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            @include('post._post-form', [
                'post_route' => isset($post) ? route('posts.update', $post->id) : route('posts.store'),
                'button_title' => isset($post) ? 'Edit' : 'Create'
            ])
        </div>
    </div>
@endsection


