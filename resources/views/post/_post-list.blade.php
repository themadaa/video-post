<tr>
    <td>
        {{$post->id}}
    </td>
    <td>
        {{$post->title}}
    </td>
    <td>
        {{$post->body}}
    </td>
    <td class="text-right">
        @include('includes._item_action', [
        'show_link_route' => route('posts.show', $post->id),
        'edit_link_route' => route('posts.edit', $post->id),
        'delete_link_route' => route('posts.destroy', $post->id),
    ])
    </td>
</tr>