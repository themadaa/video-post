@extends('layouts.main')
@section('title', 'Post')

@section('content')
    @include('includes._header', [
        'title' => $post->title,
        'second_link' => true,
        'second_link_name' => 'Posts',
        'second_link_route' => route('posts.index'),
        'current_page' => $post->title,
        'create_block' => false,
        'edit_block' => true,
        'delete_block' => true,
        'edit_link_route' => route('posts.edit', $post->id),
        'delete_link_route' => route('posts.destroy', $post->id),
    ])

    <div class="row wrapper wrapper-content animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Post</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered white-bg">
                        <tbody>
                        <tr>
                            <td><strong>Title</strong></td>
                            <td>{{ $post->title }}</td>
                        </tr>
                        <tr>
                            <td><strong>Text</strong></td>
                            <td>{!! $post->body !!}  </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Comments</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered white-bg">
                        <tbody>
                        @foreach($post->comments as $comment)
                            <tr>
                                <td>{{$comment->body}}</td>
                                <td>
                                    <form class="inline" method="POST" action="{{route('comments.destroy', $comment->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add new comment</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="POST" action="{{route('comments.post.store', $post->id)}}">
                        @csrf
                        <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                            <div class="">
                                <label class="control-label">Text</label>
                                <textarea class="form-control" name="body" rows="3">{{old('body')}}</textarea>
                                @if($errors->has('body'))
                                    <span class="help-block m-b-none">{{ $errors->first('body') }}</span>
                                @endif
                            </div>
                        </div>
                        <input class="btn btn-primary dim" type="submit" value="Add comment">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

