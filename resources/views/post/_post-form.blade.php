    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Common</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="POST" action="{{ $post_route }}">
                @if(isset($post))
                    @method('PUT')
                @endif
                @csrf
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="">
                        <label class="control-label">Title</label>
                        <input class="form-control" name="title" type="text" value="{{ old('title', isset($post) ? $post->title : null) }}">
                        @if($errors->has('title'))
                            <span class="help-block m-b-none">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                    <div class="">
                        <label class="control-label">Text</label>
                        <textarea class="form-control" name="body" rows="3">{{old('body', isset($post) ? $post->body : null)}}</textarea>
                        @if($errors->has('body'))
                            <span class="help-block m-b-none">{{ $errors->first('body') }}</span>
                        @endif
                    </div>
                </div>
                <input class="btn btn-primary dim" type="submit" value="{{$button_title}}">
                </form>
            </div>
        </div>
    </div>
