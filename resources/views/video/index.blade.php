@extends('layouts.main')
@section('title', 'Videos')

@section ('content')
    @include('includes._header', [
        'title' => 'Videos list',
        'second_link' => false,
        'current_page' => 'Videos list',
        'create_block' => true,
        'create_link_route' => route('videos.create'),
        'edit_block' => false,
        'delete_block' => false,
    ])

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Video title</th>
                                <th>Video url</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @each('video._video-list', $videos, 'video', 'video._video-empty')
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection