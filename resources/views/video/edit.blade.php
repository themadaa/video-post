@extends('layouts.main')
@section('title', 'Video edit')

@section ('content')
    @include('includes._header', [
        'title' => 'Video edit: ' . $video->title,
        'second_link' => true,
        'second_link_name' => 'Videos',
        'second_link_route' => route('videos.index'),
        'current_page' => 'Video edit',
        'create_block' => false,
        'edit_block' => false,
        'delete_block' => false,
    ])

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            @include('video._video-form', [
                'video_route' => isset($video) ? route('videos.update', $video->id) : route('videos.store'),
                'button_title' => isset($video) ? 'Edit' : 'Create'
            ])
        </div>
    </div>
@endsection


