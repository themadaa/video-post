    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Common</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="POST" action="{{ $video_route }}">
                @if(isset($video))
                    @method('PUT')
                @endif
                @csrf
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <div class="">
                        <label class="control-label">Title</label>
                        <input class="form-control" name="title" type="text" value="{{ old('title', isset($video) ? $video->title : null) }}">
                        @if($errors->has('title'))
                            <span class="help-block m-b-none">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
                    <div class="">
                        <label class="control-label">Url</label>
                        <input class="form-control" name="url" type="text" value="{{ old('url', isset($video) ? $video->url : null) }}">
                        @if($errors->has('url'))
                            <span class="help-block m-b-none">{{ $errors->first('url') }}</span>
                        @endif
                    </div>
                </div>
                <input class="btn btn-primary dim" type="submit" value="{{$button_title}}">
                </form>
            </div>
        </div>
    </div>
