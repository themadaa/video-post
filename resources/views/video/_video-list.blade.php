<tr>
    <td>
        {{$video->id}}
    </td>
    <td>
        {{$video->title}}
    </td>
    <td>
        {{$video->url}}
    </td>
    <td class="text-right">
        @include('includes._item_action', [
        'show_link_route' => route('videos.show', $video->id),
        'edit_link_route' => route('videos.edit', $video->id),
        'delete_link_route' => route('videos.destroy', $video->id),
    ])
    </td>
</tr>