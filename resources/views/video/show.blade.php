@extends('layouts.main')
@section('title', 'Video')

@section('content')
    @include('includes._header', [
        'title' => $video->title,
        'second_link' => true,
        'second_link_name' => 'Videos',
        'second_link_route' => route('videos.index'),
        'current_page' => $video->title,
        'create_block' => false,
        'edit_block' => true,
        'delete_block' => true,
        'edit_link_route' => route('videos.edit', $video->id),
        'delete_link_route' => route('videos.destroy', $video->id),
    ])

    <div class="row wrapper wrapper-content animated fadeInRight">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Video</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered white-bg">
                        <tbody>
                        <tr>
                            <td><strong>Title</strong></td>
                            <td>{{ $video->title }}</td>
                        </tr>
                        <tr>
                            <td><strong>Url</strong></td>
                            <td>{{ $video->url }}  </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <iframe width="560" height="315" src="{{ $video->url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Comments</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered white-bg">
                        <tbody>
                        @foreach($video->comments as $comment)
                            <tr>
                                <td>{{$comment->body}}</td>
                                <td>
                                    <form class="inline" method="POST" action="{{route('comments.destroy', $comment->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add new comment</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="POST" action="{{route('comments.video.store', $video->id)}}">
                        @csrf
                        <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                            <div class="">
                                <label class="control-label">Text</label>
                                <textarea class="form-control" name="body" rows="3">{{old('body')}}</textarea>
                                @if($errors->has('body'))
                                    <span class="help-block m-b-none">{{ $errors->first('body') }}</span>
                                @endif
                            </div>
                        </div>
                        <input class="btn btn-primary dim" type="submit" value="Add comment">
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

