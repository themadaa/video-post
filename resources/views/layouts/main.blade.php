<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.head')
    @include('partials.styles')
</head>
<body>
    <div id="wrapper">
        <div class="sidebar">
            @include ('partials.left-sidebar')
        </div>
        <div id="page-wrapper" class="gray-bg">
            @include('partials.navbar-header')
            @include('partials._flash')
            @yield('content')
            @include('partials.footer')
        </div>
    </div>
    @include('partials.scripts')
</body>
</html>
