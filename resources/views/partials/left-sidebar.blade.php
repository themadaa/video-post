<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="{{Route::currentRouteName() === 'home' ? 'active' : ''}}">
                <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span class="nav-label">Home</span> </a>
            </li>
            <li class="{{Route::currentRouteName() === 'posts.index' ? 'active' : ''}}">
                <a href="{{ route('posts.index') }}"><i class="fa fa-book"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="{{Route::currentRouteName() === 'videos.index' ? 'active' : ''}}">
                <a href="{{ route('videos.index') }}"><i class="fa fa-video-camera"></i> <span class="nav-label">Videos</span></a>
            </li>
        </ul>
    </div>
</nav>