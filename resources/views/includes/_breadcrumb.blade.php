<div class="col-xs-12 col-md-8">
    <h2>{{$title}}</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">Home</a>
        </li>
        @if($second_link)
            <li class="breadcrumb-item">
                <a href="{{$second_link_route}}">{{$second_link_name}}</a>
            </li>
        @endif
        <li class="breadcrumb-item active">
            <strong>{{ $current_page }}</strong>
        </li>
    </ol>
</div>
