<div class="action">
@unless(isset($not_show))
<a href="{{$show_link_route}}"
   class="btn btn-info btn-xs"><i class="fa fa-eye"></i>
</a>
@endunless
<a href="{{$edit_link_route}}"
   class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i>
</a>
<form class="inline" method="POST" action="{{$delete_link_route}}">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger btn-xs js-btn-delete"><i class="fa fa-trash"></i></button>
</form>
</div>