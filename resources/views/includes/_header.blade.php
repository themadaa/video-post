<div class="row wrapper border-bottom white-bg page-heading">
    @include('includes._breadcrumb', [
            'title' => $title,
            'second_link' => $second_link,
            'second_link_name' => $second_link_name ?? '',
            'second_link_route' => $second_link_route ?? '',
            'current_page' => $current_page,
        ])

    @include('includes._manipulate', [
            'create_block' => $create_block,
            'create_link_route' => $create_link_route ?? '',
            'edit_block' => $edit_block,
            'edit_link_route' => $edit_link_route ?? '',
            'delete_block' => $delete_block,
            'delete_link_route' => $delete_link_route ?? '',
        ])
</div>