<div class="col-xs-6 col-md-4 m-t-md">
    <div class="">
    @if($create_block)
            <a class="btn btn-primary btn-xs" href="{{$create_link_route}}">Create</a>
    @endif
    @if($edit_block)
            <a href="{{$edit_link_route}}"
               class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Edit
            </a>
    @endif
    @if($delete_block)
            <form class="inline" method="POST" action="{{$delete_link_route}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</button>
            </form>

    @endif
    </div>
</div>