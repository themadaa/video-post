<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', function () {
    return view('welcome');
}]);

Route::resource('posts', 'PostController');
Route::resource('videos', 'VideoController');
Route::post('comments/post/{post}', 'CommentController@storePost')->name('comments.post.store');
Route::post('comments/video/{video}', 'CommentController@storeVideo')->name('comments.video.store');
Route::delete('comments/{comment}', 'CommentController@destroy')->name('comments.destroy');
