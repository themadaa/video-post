<?php

use App\Models\Comment;
use App\Models\Video;
use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Video::class, 10)->create()->each(function(Video $post) {
            $counts = [2, random_int(5, 9)];
            $post->comments()->saveMany(factory(Comment::class, $counts[array_rand($counts)])->make());
        });
    }
}
