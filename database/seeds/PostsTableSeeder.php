<?php

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Post::class, 10)->create()->each(function(Post $post) {
            $counts = [2, random_int(5, 9)];
            $post->comments()->saveMany(factory(Comment::class, $counts[array_rand($counts)])->make());
        });
    }
}
