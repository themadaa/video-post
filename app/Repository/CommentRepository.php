<?php

namespace App\Repository;

class CommentRepository
{
    public function createComment($relation, $data)
    {
        $relation->comments()->create($data);

    }
    public function deleteComment($comment)
    {
        $comment->delete();
    }
}