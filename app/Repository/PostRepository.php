<?php

namespace App\Repository;

use App\Models\Post;

class PostRepository
{
    public function getAllPosts()
    {
        return Post::all();
    }

    public function createPost($data)
    {
        Post::create($data);
    }

    public function editPost($data, $post)
    {
        $post->update($data);
    }

    public function deletePost($post)
    {
        $post->comments()->delete();
        $post->delete();
    }
}