<?php

namespace App\Repository;

use App\Models\Video;

class VideoRepository
{
    public function getAllVideos()
    {
        return Video::all();
    }

    public function createVideo($data)
    {
        Video::create($data);
    }

    public function editVideo($data, $video)
    {
        $video->update($data);
    }

    public function deleteVideo($video)
    {
        $video->comments()->delete();
        $video->delete();
    }
}