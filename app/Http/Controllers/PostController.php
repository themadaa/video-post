<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Repository\PostRepository;

class PostController extends Controller
{
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }
    public function index()
    {
        $posts = $this->postRepository->getAllPosts();
        return view('post.index', compact('posts'));
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(PostRequest $request)
    {
        $data = $request->except('_token');
        $this->postRepository->createPost($data);
        return redirect()->route('posts.index')->with('success', "Post is successfully created");
    }

    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('post.edit', compact('post'));
    }

    public function update(PostRequest $request, Post $post)
    {
        $data = $request->except('_token');
        $this->postRepository->editPost($data, $post);
        return redirect()->route('posts.index')->with('success', "Post is successfully edited");
    }

    public function destroy(Post $post)
    {
        $this->postRepository->deletePost($post);
        return redirect()->route('posts.index')->with('success', "Post is successfully deleted");
    }
}
