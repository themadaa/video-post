<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\{Comment, Video, Post};
use App\Repository\CommentRepository;

class CommentController extends Controller
{
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function storePost(CommentRequest $request, Post $post)
    {
        $data = $request->except('_token');
        $this->commentRepository->createComment($post, $data);
        return redirect()->back()->with('success', "Comment is successfully created");
    }

    public function storeVideo(CommentRequest $request, Video $video)
    {
        $data = $request->except('_token');
        $this->commentRepository->createComment($video, $data);
        return redirect()->back()->with('success', "Comment is successfully created");
    }

    public function destroy(Comment $comment)
    {
        $this->commentRepository->deleteComment($comment);
        return redirect()->back()->with('success', "Comment is successfully deleted");
    }
}
