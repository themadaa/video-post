<?php

namespace App\Http\Controllers;

use App\Http\Requests\VideoRequest;
use App\Models\Video;
use App\Repository\VideoRepository;

class VideoController extends Controller
{
    private $videoRepository;

    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    public function index()
    {
        $videos = $this->videoRepository->getAllVideos();
        return view('video.index', compact('videos'));
    }

    public function create()
    {
        return view('video.create');
    }

    public function store(VideoRequest $request)
    {
        $data = $request->except('_token');
        $this->videoRepository->createVideo($data);
        return redirect()->route('videos.index')->with('success', "Video is successfully created");
    }

    public function show(Video $video)
    {
        return view('video.show', compact('video'));
    }

    public function edit(Video $video)
    {
        return view('video.edit', compact('video'));
    }

    public function update(VideoRequest $request, Video $video)
    {
        $data = $request->except('_token');
        $this->videoRepository->editVideo($data, $video);
        return redirect()->route('videos.index')->with('success', "Video is successfully edited");
    }

    public function destroy(Video $video)
    {
        $this->videoRepository->deleteVideo($video);
        return redirect()->route('videos.index')->with('success', "Video is successfully deleted");
    }
}
